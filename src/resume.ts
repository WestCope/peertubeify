import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';
import Preferences from './preferences';

interface VideoEntry {
    readonly uuid: string,
    currentTime: number,
}

export class ResumeManager {
    private shouldResume: boolean

    constructor (shouldResume) {
        this.shouldResume = shouldResume;
    }

    resume() {
        if (!this.shouldResume) {
            return;
        }

        resumeVideo();
    }

    start() {
        window.setInterval(updateStorage, 1000)
    }

    static async create() {
        const prefs = await Preferences.getPreferences();
        return new ResumeManager(prefs.resumeUncompletedVideo);
    }
}

function getVideoElement () {
    return document.getElementById('vjs_video_3_html5_api') as HTMLVideoElement;
}

async function resumeVideo() {
    const videoElement = getVideoElement();
    const storage = await browser.storage.local.get('uncompletedVideos');
    const uncompletedVideos: Array<VideoEntry> = storage.uncompletedVideos;
    const uuid = _.last(_.split('/', location.href));

    const savedEntry = _.find((e: VideoEntry) => e.uuid == uuid, uncompletedVideos);

    if (savedEntry) {
        videoElement.currentTime = savedEntry.currentTime;
        browser.storage.local.set({ uncompletedVideos });
    } else {
        window.setTimeout(resumeVideo, 1000);
    }
}

async function updateStorage() {
    const videoElement = getVideoElement();
    const storage = await browser.storage.local.get('uncompletedVideos');
    const uuid = _.last(_.split('/', location.href));

    const uncompletedVideos: Array<VideoEntry> = storage.uncompletedVideos || [];

    if (!videoElement.ended) {
        const entry: VideoEntry = {
            uuid,
            currentTime: videoElement.currentTime
        };

        const savedEntry = _.find((e: VideoEntry) => e.uuid == uuid, uncompletedVideos);

        if (savedEntry) {
            savedEntry.currentTime = entry.currentTime;
        } else {
            uncompletedVideos.push(entry);
        }
    } else {
        const index = _.findIndex((e: VideoEntry) => e.uuid == uuid, uncompletedVideos);
        if (index !== -1) {
            uncompletedVideos.splice(index, 1);
        }
    }

    browser.storage.local.set({ uncompletedVideos });
};
