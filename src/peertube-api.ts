/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import constants from './constants';
import Preferences from './preferences';

async function fetchAPI(path: string, query?: Object) {
    const instance = (await Preferences.getPreferences()).searchInstance;

    let url = `https://${instance}/${constants.peertubeAPI.endpoint}/${path}`;

    if (query) {
        url = url + '?' + Object.keys(query).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(query[key])).join('&');
    }

    return fetch(url)
        .then(res => res.json())
        .catch(e => console.error(
            `An error occured while trying to fetch ${instance} API used by PeerTubeify: `
            + e.message
        ));
}

export async function getVideo(id: string) {
    return fetchAPI('videos/' + id);
}

export async function searchVideo(query: Object) {
    return fetchAPI('search/videos', query);
}
